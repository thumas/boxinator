import { createSlice } from "@reduxjs/toolkit"; 


const initialState = {
    receiver: 'default'
}
export const receiverSlice = createSlice ({
    name: 'receiverSlice',
    initialState,
    reducers: {
        setReceiver: (state, action) => {
            state.receiver = action.payload
        }
    }
})

export const { setReceiver} = receiverSlice.actions

export default receiverSlice.reducer;
