import { createSlice } from "@reduxjs/toolkit"; 



export const colorSlice = createSlice ({
    name: 'colorSlice',
    initialState: { value: 'red'},
    reducers: {
        setColor: (state, action) => {
            state.value = action.payload
            console.log(action.payload)
        }
    }
})

export const { setColor } = colorSlice.actions

export default colorSlice.reducer;
