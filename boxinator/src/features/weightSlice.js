import { createSlice } from "@reduxjs/toolkit"; 


const initialState = {
    weight: 1,
}
export const weightSlice = createSlice ({
    name: 'weightSlice',
    initialState,
    reducers: {
        setWeight: (state, action) => {
            state.weight = action.payload
        }
    }
})

export const { setWeight} = weightSlice.actions

export default weightSlice.reducer;
