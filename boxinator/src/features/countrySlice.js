import { createSlice } from "@reduxjs/toolkit"; 


const initialState = {
    country: 'default'
}
export const countrySlice = createSlice ({
    name: 'countrySlice',
    initialState,
    reducers: {
        setCountry: (state, action) => {
            state.country = action.payload
            console.log(action.payload)
        }
    }
})

export const { setCountry } = countrySlice.actions

export default countrySlice.reducer;
