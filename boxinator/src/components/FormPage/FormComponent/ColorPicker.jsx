import React from "react";
import {useSelector, useDispatch} from "react-redux"
import { ChromePicker } from "react-color";
import '../../../App.css'
import { setColor } from "../../../features/colorSlice";

const ColorPicker = () => {
    const dispatch = useDispatch()
    const color = useSelector((state ) => state.color.value)
    
    return(
        <div>
            <ChromePicker disableAlpha color={color} onChange = {updateColor => dispatch(setColor(updateColor.rgb))}/>
        </div>
    )

   
}

export default ColorPicker