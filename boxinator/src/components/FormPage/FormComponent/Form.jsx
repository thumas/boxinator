import React, {useState} from 'react'
import { useSelector, useDispatch, } from 'react-redux'
import { setReceiver } from '../../../features/receiverSlice'
import { setWeight } from '../../../features/weightSlice'
import axios from 'axios'
import  ColorPicker  from './ColorPicker'
import CountryPicker from './CountryPicker'
import { Link } from 'react-router-dom'


const Form = () => {

      let rec = useSelector(state => state.receiver)
      let wei = useSelector(state => state.weight)
      let col = useSelector(state => state.color)
      let coun = useSelector(state => state.country)

      const [hidden, hide] = useState(false)
      

      const sumbitBox = () => {
      console.log("test-receiver: ", rec.receiver)
      
        console.log("test-weight: ", wei.weight)
        console.log("test-color: ", col)
        console.log("r: ", col.value.r)
        console.log("g: ", col.value.g)
        console.log("b: ",col.value.b)
        axios.post('http://localhost:3001/api/insert/', {
          receiver: rec.receiver,
          weight: wei.weight,
          r: col.value.r,
          g: col.value.g,
          b: col.value.b,
          country: coun.country
        }).then(() => {
          console.log('posted')
        }).catch(err => console.error(err));
         }
        const dispatch = useDispatch()
      

    
    return (
       
        <div className="App">
        <h1> Boxinator </h1>
        <Link to='/listboxes' style={{ textDecoration: 'none', fontSize: '25px' }}>Show Shipments</Link> <br/>
        <div className='form'>
            <label> Receiver name: </label>
            <input type="text" name= "receiver" onChange={(e) => {
            dispatch(setReceiver(e.target.value))
            }}></input>
            <label> Weight (kg): </label>
            <input type="number" name= "weight" onChange={(e) => {
            dispatch(setWeight(e.target.value))
            }}></input>
            <button type="button" class="btn btn-light" onClick={() => hide(!hidden)}> Pick color </button>
            {hidden && <ColorPicker />}
            <label htmlFor="countries">Country:</label>
            <CountryPicker/>
            
            <div>
            <button type="button" class="btn btn-success mt-2" onClick={sumbitBox}>Save</button>
            </div>
           
        </div>
    </div>
    )
}

export default Form