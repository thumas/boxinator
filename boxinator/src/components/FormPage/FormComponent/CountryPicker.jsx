import React from "react";
import {useSelector, useDispatch} from "react-redux"
import '../../../App.css'
import { setCountry } from "../../../features/countrySlice";
import Select from 'react-select';


const CountryPicker = () =>  {
    const options = [
        { value: 'sweden', label: 'Sweden' },
        { value: 'australia', label: 'Australia' },
        { value: 'brazil', label: 'Brazil' },
        { value: 'china', label: 'China'}
      ];
    
    const dispatch = useDispatch()
    const country = useSelector((state ) => state.country.value)

    return (
      <Select
        value={country}
        onChange={e => dispatch(setCountry(e.value))}
        options={options}
      />
    )
  
}
export default CountryPicker