import react, {useEffect, useState} from "react"
import axios from "axios"
import './List.css'
import { Link } from "react-router-dom"


const List = () => {

    const [shippingList, setShippingList] = useState([])
    let totalweight = 0
    let totalcost = 0

    useEffect(() => {
        axios.get('http://localhost:3001/api/get/').then((response) => {
           setShippingList(response.data)
        })
    })

    const totalCalc = () => {
        shippingList.forEach(element => {
            totalweight += element.weight
            totalcost += (element.weight * element.multiplier)
        });
    }
    totalCalc()

    return (
        <div>   
            <Link to='/addbox' style={{ textDecoration: 'none', fontSize: '25px' }}> Go Back </Link> <br/>
            <div class= "row border border-secondary">
                        <div class="col">
                        <b> Receiver </b>
                        </div>
                        <div class="col">
                            <b> Weight </b>
                        </div>
                        <div class="col">
                            <b> Color </b>
                        </div>
                        <div class="col">
                            <b> Country </b> 
                        </div>
                        <div class="col">
                            <b> Cost </b>
                            </div>
                    </div>
            {shippingList.map((val) => {
            return(
                    <div class= "row border border-secondary">
                        <div class="col">
                        <p> {val.receiver} </p>
                        </div>
                        <div class="col">
                            <p>  {val.weight} </p>
                           
                        </div>
                        <div class="col" style={{ backgroundColor: `rgb(${val.color.r},${val.color.g},${val.color.b})` }}>
                          
                        </div>
                        <div class="col">
                            <p>{val.country}</p> 
                        </div>
                        <div class="col">
                            <p>{Math.round(val.weight * val.multiplier)} SEK </p>
                            </div>
                            
                    </div>
              
            ) 
        })}

        <div> 
            <b>Total weight: {totalweight}</b><br/>
            <b>Total cost: {Math.round(totalcost)} SEK </b><br/>
        </div>


        
        </div>
    )
}

export default List