import React from 'react'
import './App.css'
import List from './components/ListPage/ListComponent/List'
import {
  HashRouter,
  Routes,
  Route,
} from 'react-router-dom'
import Form from './components/FormPage/FormComponent'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.min.js"



const App = () => {


  return (
    <HashRouter>
    <Routes>
    <Route
      exact
      path='/'
      element={<Form/>}
    />
      <Route
      exact
      path='/addbox'
      element={<Form/>}
    />
    <Route
      exact
      path='/listboxes'
      element={<List/>}
    />
    </Routes>
  </HashRouter>

  );
}

export default App;
