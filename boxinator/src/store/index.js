import {configureStore } from '@reduxjs/toolkit'
import receiverSlice from '../features/receiverSlice'
import weightSlice from '../features/weightSlice'
import colorSlice from '../features/colorSlice'
import countrySlice from "../features/countrySlice"

export const store = configureStore({
    reducer: {
        receiver: receiverSlice,
        weight: weightSlice,
        color: colorSlice,
        country: countrySlice
    }
})