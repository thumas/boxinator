// "Imports"
const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const mysql = require('mysql')

// Select port to run on
const port = 3001

// Set these to match current shipping consts
const swedenMultiplier = 1.3
const chinaMultiplier = 4
const brazilMultiplier = 8.6
const australiaMultiplier = 7.2

// MySQL user credentials
const db = mysql.createPool({
    host: 'localhost', 
    user: 'root',
    password: 'password',
    database: 'boxdb'
})

app.use(cors()) // Cors
app.use(express.json()) // Allows reading json from the frontend
app.use(bodyParser.urlencoded({extended: true})) // 

// API Get 
app.get('/api/get', (req, res) => {
    const sqlSelect = "SELECT * FROM boxes"
    db.query(sqlSelect, (err, result) => {
        res.send(formatter(result))
    })
})

// API Post
app.post("/api/insert", (req, res) => {

    const receiver = req.body.receiver
    const weight = req.body.weight
    const r = req.body.r
    const g = req.body.g
    const b = req.body.b
    const country = req.body.country

    const sqlInsert = "INSERT INTO boxes (receiver, weight,r ,g, b, country) VALUES (?,?,?,?,?,?)"
    db.query(sqlInsert, [receiver, weight,r,g,b, country], (err, result) => {
        if (err){
            console.log(err)
        }
        
    })
})

// Formats sql to match front-end needs, sets shipping multipliers. Not pretty, I'm not experienced in the area =)
const formatter = (entry) => {
    const result = []
    entry.forEach(element => {
        let calcMultiplier = 0
        switch (element.country){
            case "sweden": calcMultiplier = swedenMultiplier 
                break;
            case "china": calcMultiplier = chinaMultiplier
                break;
            case "brazil": calcMultiplier = brazilMultiplier
                break;
            case "australia": calcMultiplier = australiaMultiplier
                break; 
            default: calcMultiplier = 0
        }
        resObject = {
            receiver: element.receiver,
            weight: element.weight,
            color: {r: element.r,
                    g: element.g,
                    b: element.b},
            country: element.country,
            multiplier: calcMultiplier
        } 
        result.push(resObject)
    });
   
    return result
}

// Listener
app.listen(port, () =>  {
    console.log(`running on port ${port}`)
})