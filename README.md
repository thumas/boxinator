## Boxinator
Made by Thomas Annerfeldt https://gitlab.com/thumas/

Commit history: https://gitlab.com/thumas/boxinator

## Installation

1. Install MySQL and run the script in /server/MySQL

2. npm install

3. set user credentials for MySQL in /server/index.js

4. Pick a port in /server/index.js

## Contact

Email: thomas.annerfeldt@gmail.com
LinkedIn: https://www.linkedin.com/in/thomasannerfeldt/